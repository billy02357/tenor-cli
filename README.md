# tenor-cli

"Scrape" tenor.com and download gifs easily from the commandline

## Dependencies
```
curl OR wget
sxiv
```

## Usage
```
tenor-cli [query] [path]
```

### Examples
I hate documentation with no examples!
```
tenor-cli "pepe" ~/pictures/pepe/
```
