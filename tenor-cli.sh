#!/bin/sh

set -e


printUsage() {
	echo "Usage: $0 [query] [no-verbose]"
	exit 1
}

if test "$1" = "-h" || test "$1" = "--help" || test "$1" = "h" || test "$1" = "help" || test -z "$1"; then
	printUsage
fi

urls="$(curl -s "https://tenor.com/search/$1-gifs" | pup | tr ';' '\n' | sed 's/&lt//g' | sed 's/&gt//g' | sed 's/&#34//g' | grep "/view")"
view_urls="$(echo "$urls" | sed 's/ /\n/g' | sed 's/\/view\///g')"

tmpdir="$(mktemp -d)"
cd "$tmpdir" 1>/dev/null 2>&1  || exit 1
start=$(date +%s.%N)
echo "Downloading files"
echo "Please wait for the keks..."
for url in $view_urls; do
	gif_url="https://tenor.com/view/${url}"
	basename="$(basename "$gif_url")"
	gif_name="${basename%-*}"
	full_gif_file_url="$(curl -s "$gif_url" | pup | tr ';' '\n' | sed 's/&lt//g' | sed 's/&gt//g' | sed 's/&#34//g' | grep "^https://c.tenor.com/" | head -n 1)"
	if test -n "$(command -v curl)"; then
		if test "$2" != "noverbose" && test "$2" != "no-verbose" && test "$2" != "-no-verbose" && test "$2" != "--no-verbose"; then
			echo "cURLing le $gif_name"
		fi
		curl -s "$full_gif_file_url" > "$gif_name.gif" &
	elif test -n "$(command -v wget)"; then
		if test "$2" -ne "noverbose" && test "$2" -ne "no-verbose" && test "$2" -ne "-no-verbose" && test "$2" -ne "--no-verbose"; then
			echo "wgetting le $gif_name"
		fi
		wget --quiet "$full_gif_file_url" -O "$gif_name.gif" &
	fi
done
wait
duration="$(echo "$(date +%s.%N) - $start" | bc)"
execution_time="$(printf "%.2f seconds" "$duration")"
echo "Took $execution_time"

kekd="$(sxiv -t -a -o ./*)"

cd - 1>/dev/null 2>&1

for f in $kekd; do
	mv "$tmpdir/$f" "." 1>/dev/null 2>&1
done

rm -rf "$tmpdir" 1>/dev/null 2>&1
